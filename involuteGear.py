import bpy
import math

# Specification
# Gear size is (z + 2) * m [milimeter]
# Same value is required for m, every engaged gears.
z = 61 # Relatively prime number, every engaged gears.
m = 1 # 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1, 1.25, 1.5, 2, 2.5, 3, 4, 5, 6, 8, 10, 12, 16, 20, 25, 32, 40, 50
alpha = 10
extrude = 0.1 # [meter]

def createObject(name, coords, view_layer):
    curveData = bpy.data.curves.new(name, type='CURVE')
    curveData.dimensions = '2D'
    curveData.resolution_u = 100
    curveData.bevel_depth = 0
    curveData.fill_mode = 'BOTH'
    curveData.extrude = 1

    mapCoordsToSpline(coords, curveData)

    # create Object
    curveObject = bpy.data.objects.new(name, curveData)
    curveObject.location = (0, 0, 0)

    # Link curve object to the active collection of current view layer
    view_layer.active_layer_collection.collection.objects.link(curveObject)

def calcInvA(base, r):
    a = math.acos(base / r)
    inv_a = math.tan(a) - a
    return inv_a

def involuteCurveRkToRb(rk, rb):
    coords = []
    N = 10
    for i in range(N):
        r = rb + (rk - rb) * i / (N - 1)
        inv_a = calcInvA(rb, r)
        x = r * math.cos(inv_a)
        y = r * math.sin(inv_a)
        coords.append((x, y, 0))
    return coords

def circleCurve(r, theta_a, theta_b):
    coords = []
    N = 10
    for i in range(N):
        theta = theta_a + (theta_b - theta_a) * i / (N - 1)
        x = r * math.cos(theta)
        y = r * math.sin(theta)
        coords.append((x, y, 0))
    return coords

# map coords to spline
def mapCoordsToSpline(coords, curveData):
    polyline = curveData.splines.new('POLY')
    polyline.use_cyclic_u = True
    polyline.points.add(len(coords) - 1)

    for i, coord in enumerate(coords):
        x,y,z = coord
        polyline.points[i].co = (x, y, z, 0)

view_layer = bpy.context.view_layer

# Basic circle
dp = z * m
dk = dp + 2 * m
df = dp - 2.5 * m
db = dp * math.cos(2 * math.pi * alpha / 360)

print(dp, dk, df, db)

### theta = 0
coords0 = [(df / 2, 0, 0), (db / 2, 0, 0)]

### 0 <= theta < thetaP
coords1 = involuteCurveRkToRb(dk / 2, db / 2)

### thetaP < theta < thetaQ
thetaP = calcInvA(db / 2, dk / 2)
thetaQ = math.pi / z - thetaP
coords2 = circleCurve(dk / 2, thetaP, thetaQ)

def rotateOnXY(x, y, z, theta):
    return (x * math.cos(theta) - y * math.sin(theta),
            x * math.sin(theta) + y * math.cos(theta),
            0)

### thetaQ < theta < thetab
thetab = math.pi / z + 2 * calcInvA(db / 2, dp / 2)
coords3 = list(map(lambda coord: rotateOnXY(coord[0], -coord[1], 0, thetab), coords1))
coords3.reverse()

### thetab < theta < theta_end
theta_end = 2 * math.pi / z
coords4 = circleCurve(df / 2, thetab, theta_end)

coords = coords0 + coords1 + coords2 + coords3 + coords4
gear = []
for i in range(z):
    theta = 2 * math.pi * i / z
    gear = gear + list(map(lambda coord: rotateOnXY(coord[0], coord[1], 0, theta), coords))

createObject("Involute_Gear_M=" + str(m) + ",z=" + str(z), gear, view_layer)
