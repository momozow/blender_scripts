import bpy
import math

class Pallet():
    def make(name, view_layer):
        # Specification
        r = 20
        z = 5
        phi = 15 / 360 * 2 * math.pi
        alpha = 5
        beta = 5

        ra = r * 1.1

        A = (0, r + z,0 )
        B = (  ra * math.cos(-phi), r + z + ra * math.sin(-phi), 0)
        b = (- ra * math.cos(-phi), r + z + ra * math.sin(-phi), 0)
        C = (  r - alpha, beta, 0)
        c = (- r + alpha, beta, 0)

        coords0 = [c, b, A, B, C]
        return(coords0)
