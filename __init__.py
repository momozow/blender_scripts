import sys, os
import bpy

script_filename = os.path.basename(__file__)
script_filepath = bpy.data.texts[script_filename].filepath
script_dirpath = os.path.dirname(script_filepath)
sys.path += [script_dirpath]

from clockparts import pendullum

def createObject(name, coords, fill, enclose, view_layer):
    curveData = bpy.data.curves.new(name, type='CURVE')
    curveData.dimensions = '2D'
    curveData.resolution_u = 100
    curveData.bevel_depth = 1
    curveData.fill_mode = fill
    curveData.extrude = 1

    polyline = curveData.splines.new('POLY')
    polyline.use_cyclic_u = enclose
    polyline.points.add(len(coords) - 1)

    for i, coord in enumerate(coords):
        x,y,z = coord
        polyline.points[i].co = (x, y, z, 0)

    curveObject = bpy.data.objects.new(name, curveData)
    curveObject.location = (0, 0, 0)

    view_layer.active_layer_collection.collection.objects.link(curveObject)

if __name__ == "__main__":
    view_layer = bpy.context.view_layer

    coords = pendullum.Pallet.make("pallet", view_layer)
    createObject("pallet", coords, 'NONE', False, view_layer)
